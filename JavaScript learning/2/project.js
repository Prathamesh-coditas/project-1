// const id = document.getElementById("main-heading");  id selector
const id = document.querySelector("#main-heading");//query selector. As we are selecting id so put # before id.
//console.log(id);     //it returns entire element having particular id
console.dir(id);////it will return an object which will contain all properties

/*document.querySelector  vs  document.querySelectorAll
=>document.querySelector will select 1st class which is encountered 
  But document.querySelectorAll will select all classes having that class 
*/

const heading = document.querySelector("#main-heading");
console.log(heading.textContent);
heading.textContent = "This is new heading";
console.log(heading.textContent);

const link = document.querySelector("a");
link.setAttribute("href","https://css-tricks.com/snippets/css/complete-guide-grid/");

const changeAnchors = document.getElementsByTagName("a");
for(let tags of changeAnchors){
    tags.style.backgroundColor = "white";
    tags.style.color = "green";
    tags.style.fontWeight = "bold";
}


const container = document.querySelector(".container");
console.log(container.children);

console.log(container.children[1]);



const header = document.querySelector(".header");

console.log(header.classList);

const todoList = document.querySelector(".todo-list");
todoList.insertAdjacentHTML("afterbegin", "<li>Teach Students </li>");


const ul = document.querySelector(".todo-list");
const li = document.createElement("li");
li.textContent = "new todo";
const li2 = li.cloneNode(true);//deep cloning.Means node will be cloned along with its children.
ul.append(li);//added after
ul.prepend(li2);//added before

const sectionTodo = document.querySelector(".section-todo");
const info = sectionTodo.getBoundingClientRect();
console.log(info);

//============================================================
const btn = document.querySelector(".btn-headline");
btn.onclick = function clickMe(){ // Here we are assigning fn to button.This fn is executed after clicking this button
    console.log("you clicked me !!!!!");
}

//============================================================
const btn_1 = document.querySelector(".btn-headline");

btn.addEventListener("click",function(){
    console.log("you clicked me !!!!");
    console.log("value of this")
    console.log(this);
});

//==============================================================
const allButtons = document.querySelectorAll(".my-buttons button");//It'll select all buttons and put it in allButtons


for(let button of allButtons){
    button.addEventListener("click",(e)=>{ //we are adding event to buttons when it is clicked
        console.log(e.currentTarget); //currentTarget property always refers to the element whose event listener triggered the event
    })
}


