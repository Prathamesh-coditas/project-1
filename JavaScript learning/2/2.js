//var can be accessed before initializing
console.log(firstName);
var firstName = "pratham";
console.log(firstName);

console.log(myFunction); 
function myFunction(){
    console.log("this is my function");
}


console.log(myFunction1);
var myFunction1 = function(){
    console.log("this is my function");
}
console.log(myFunction1);

/*
Function execution context
*/
let variable1 = "variable1";//uninitialized during creation phase
console.log(variable1);
function getFullName(firstName, lastName){
    console.log(arguments);
    let myVar = "var inside func";
    console.log(myVar);
    const fullName = firstName + " " + lastName;
    return fullName;
}

const personName = getFullName("harshit", "sharma");//When we call fn then a fn executn context is created.
console.log(personName);

//==============================================================

const lastName = "Londhe";

const printName = function(){
    const firstName = "Pratham";
    function myFunction(){
        console.log(firstName);//we'll get firstName from the fn
        console.log(lastName);/*As lastName is not present locally, It'll be accessed from parent 
        i.e. global exectn context here.But if variable is present locally then that local variable will be accesed.
    */ 
    }
    myFunction()
    
}
printName();

/*
closures
When a fn returns a fn then it will take all local variables(that fn is using) along with it.
*/
function outerFunction(){
    function innerFunction(){
        console.log("hello world");
    }
    return innerFunction;
}

const ans = outerFunction();
// console.log(ans);    It'll print whole fn
ans();//    It'll print "hello world"

//===========================================================
function printFullName(firstName, lastName){
    function printName(){
        console.log(firstName, lastName);
    }
    return printName;//when printName is returned it will take all variables it is using along with it.Here firstName and lastName
}

const ans1 = printFullName("harshit", "sharma");
// console.log(ans);
ans();

//==========================================================
function myFunction(power){
    return function(number){
        return number ** power
    }
}
const square = myFunction(2);//setting power as 2
const ans3 = square(3);//square has stored fn in it to which we passed number 3.ans3 will be calculated as 3^2 
console.log(ans3);


const cube = myFunction(3);//setting power as 3
const ans2 = cube(3);//cube has stored fn in it to which we passed number 3.ans3 will be calculated as 3^3
console.log(ans2);

//==========================================================
let flag = 0;
function myFunc(){
    return function(){
        if(flag == 0){
            console.log("1st time");
            flag=1;
        }else{
            console.log("already called");
        }
    }
}

const answer = myFunc();
answer();
answer();
answer();
