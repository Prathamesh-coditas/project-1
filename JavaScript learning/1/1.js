"use strict";
//console.log
console.log("Hello world");

/*
var declarations
*/
var fName = "Pratham";
console.log(fName);
var fName = "Prathamesh";
console.log(fName);

/*
const declarations
*/
const myName = "Prathamesh"
// myName = "Raghav";       it will give error as const declarations cant be reassigned


/*
let declarations

*/
let firstName = "Pratham          ";

//lenght of string
console.log(`Length of a string :${firstName.length}`);

//remove whitespaces
firstName = firstName.trim();
console.log(`Length after removing white-spaces:${firstName.length}`);

//slice() to get a substring
let slicedString = firstName.slice(0, 4);
console.log(`Substring or sliced string is:${slicedString}`);

//string to number and number to string
let str = "34";
console.log(typeof str);
str = +str;// just add + symbol before a string to convert a string into number
console.log(typeof str);

let num = 21;
//num = String(num);    Explicit casting into string type
num = num + "";//just add "" after a number rto convert a string into number
console.log(typeof num);

/*
BigInt
*/
let myNum = 123;
console.log(`type of 123 is ${typeof myNum}`);

myNum = BigInt(1234567898765432345678);
console.log(`type of 1234567898765432345678 is ${typeof myNum}`);

/*
== vs ===
*/
let num1 = 7;
let str1 = "7";
console.log(num1 == str1);//Will return true cz it just checks value irrespective of datatype
console.log(num1 === str1);//will return false cz it checks value as well as datatype
console.log(num1 != str1);//returns true just by checking values
console.log(num1 !== str1);//returns false by checking values as well as datatypes

/*
Ternary Operator
*/
let age = 5;
let drink = age > 5 ? "Coffee" : "milk";
console.log(drink);

/*
User input
It always takes string as an input
*/
// let userInput = prompt("Enter a number");

/*
switch()
*/
let day = 5;

switch (day) {
    case 0:
        console.log("Sunday");
        break;

    case 1:
        console.log("Monday");
        break;

    case 2:
        console.log("Tuesday");
        break;

    case 3:
        console.log("Wednesday");
        break;

    case 4:
        console.log("Thursday");
        break;

    case 5:
        console.log("Friday");
        break;

    case 6:
        console.log("Saturday");
        break;
}

/*
Array in JS may contain different datatypes in it.
Arrays are mutable in js.Changes made will be directly reflected in array.
*/
let arr = ["Apple",2,3,undefined,"Grapes"];
console.log(`original array: ${arr}`);

arr.push("banana");//adds element in arr at last.
console.log(`array after push: ${arr}`);

console.log(`poped element is: ${arr.pop()}`);//removes last element in array and also returns it.
console.log(`array after pop: ${arr}`);

arr.unshift(10);//adds element at first position i.e. 0th index position.
console.log(`array after unshift: ${arr}`);

arr.shift();//removes element present at first position i.e. 0th index position and also returns it.
console.log(`array after shift: ${arr}`);


/*
primitive types vs reference types
*/
let number = 12;
let number2 = number;/*as number is primitive type 2  different copies of number and number2 will be created and both
varriables will point to different locations in memory location.   
*/
console.log(`value of number is : ${number}`);
console.log(`value of number2 is : ${number2}`);

number++;number++;

console.log("After incr of number");
console.log(`value of number is : ${number}`);
console.log(`value of number2 is : ${number2}`);
///////////////////////////////////////////////////////////////////////////////////////////////////
let arr1 = ["item1","item2"];
let arr2 = arr1;/*
as array is reference type both arr1 and arr2 will refer to same array created in memory thus changes made in arr1 will
directly be reflected in arr2
*/
console.log(`arr1 : ${arr1}`);
console.log(`arr2 : ${arr2}`);

arr1.push("item3");
console.log("After pushing item3 in arr1");
console.log(`arr1 : ${arr1}`);
console.log(`arr2 : ${arr2}`);

/*
shorthand to access array values i.e. destructuring
*/
let arr3 = ["value1","value2","value3"];
let[a1,a2,a3,a4] = arr3;//1st value is assigned to a1,2nd value to a2 and so on
console.log(a4);//it will give undefined
let[a11, ,a13] = arr3;//value1 is assigned to a11,value to will not be assigned to anything and value3 is assigned to a13
let[a21,...a22] = arr3;//1st value is assigned to a21 and all remaining values go into a22
console.log(a22);

/*
object
stores key value pairs
keys are always in string
*/

const person = {name:"pratham",age:21,languages:["c#","Java","Js"]};
const getArr = person.languages;
for(var item of getArr){/*for..in iterates over all enumerable property keys of an object. 
for..of iterates over the values of an iterable object. */
    console.log(`Languages stored in object are : ${item}`);
}
/*
Addition of extra key in object after creating object
*/
const key = "email";
person[key] = "pratham@gmail.com";
console.log(person);
/*
retrieval of data from object 
*/
for(let key in person){
    // console.log(`${key} : ${person[key]}`);
    console.log(key,":",person[key]);
}

const alphabets = {..."abcdefghijklmnopqrstuvwxyz"};
console.log(alphabets);/*
alphabets will be an object that stores a to z where
0:a,1:b,2:c and so on i.e. 25:z
Here 0 is a key and value is a 
similaraly 1 is a key and value corresponding to it is b
*/
console.log(alphabets[15]);//It will print value of key 15


/*
Functions
*/
function sumOFNumbers(num1 , num2){
return num1 + num2;
}
console.log(`sum of 2 numbers is : ${sumOFNumbers(12,13)}`);

//Function expression means assigning a function to a variable
const myFunction = function(){
    console.log("Hello World !!!");
}
myFunction();

/*
Arrow Functions 
steps
1)Write function in function expression format
2)Remove function keyword
3)give => after ()
*/
const isAdult = (age) => {
    if(age>18){
        return "adult";
    }
    return "minor";
}
console.log(isAdult(19));


/*
Hoisting
function can be called before declaring only if we declare function in this format
if function is declared in format of function expression then hoisting is not possible
*/
hello();
function hello(){
    console.log("Hello called");
}

/*
function inside function
*/
function outerFunc(){
    const inner1 = function(a,b){
        return a+b;
    }

    console.log("outer function");
    console.log(`Addition given by inner function is ${inner1(7,8)}`);
}
outerFunc();

/*
let and const are block scope
var has function scope
*/
if(true){
    let _variable1 = "let inside block";
    var _variable2 = "var inside block";
    console.log(_variable1);
}
// console.log(_variable1); this _variable1 cant be accesed as it is of block scope.Same is applied for const
console.log(_variable2);//Its scope is within complete function so it can be accesed outside block

/*
Default values
*/
function sum(a,b=1){/* if value for b is not provided then it will automatically take b=1.But is value is provided
explicitely then default value doesn't have any effect */
    return a+b;
}
console.log(sum(2,9));
    
/*
Callback function
*/
function mainFunc(){
    console.log("main func called");
}

function callBackFunc(a){
    console.log(a);//it will print complete fn
    mainFunc();//it will execute mainFunc() 
}
callBackFunc(mainFunc);//we are passing fn as a paramt to another fn.

/*
shorthand for passing each value in array to function
*/
let arr4 = [2,4,6,8,10];
function myFunction1(number , index){
    console.log(`actual value is ${number} and index position is ${index}`);
}
arr4.forEach(myFunction1);/*it will run for-each loop on arr and automatically pass arguments where
1st argument is always actual value at index position and 2nd args will be an index position
if we pass 3rd args then it will be the complete array
*/


/*
map()
*/
const myArr1 = [1,3,4,5,6];
const sq = function(number){
   return number%2 === 0;
}
const single_arr = myArr1.map(sq);/*map() performs operation according to function passed and store 
the returned values in new array.As function returns boolean value,boolean values will be stored 
in new array.
*/
console.log(single_arr);

/*
filter()
*/
const myArr2 = [1,3,4,5,6];
const sq2 = function(number){
   return number%2 !== 0;//returns true when number is odd
}
const single_arr1 = myArr1.filter(sq2);/*filter() performs operation according to function passed and store 
actual element in new array.If function returns true then value corresponding to that iteration will 
be stored in new array 
*/
console.log(single_arr1);


/*
sort()
sort() will first convert everything to string and then sort it according to ascii values.
*/
const numbers = [5,9,1200, 410, 3000];
numbers.sort((a,b)=>{
    return a-b;//b-a will sort in descending order & a-b will sort in ascending order
});
console.log(numbers);

/*
find()
once condition is satisfied,it'll not check further
*/
const users = [
    {userId : 1, userName: "harshit"},
    {userId : 2, userName: "harsh"},
    {userId : 3, userName: "nitish"},
    {userId : 4, userName: "mohit"},
    {userId : 5, userName: "aaditya"},
];
const myUser = users.find((user)=>{return user.userId===3;});
console.log(myUser); 

/*
fill()
here we are filling 0 from 2nd index position to 4th index position
*/
const myArray = [1,2,3,4,5,6,7,8];
myArray.fill(0,2,5);
console.log(myArray);


/*
splice()
splice(start_index , number_of_items_to_delete ,item_to_insert);
It changes original array
*/
const myArray3 = ["item1","item2","item3"];
myArray3.splice(1,1);//here we are specifying where to start and how much items to delete from an array
console.log(myArray3);

myArray3.splice(2,0,"inserted items");//at 2nd index position we are not deleting anything but inserting an item
console.log(myArray3);

/*
map structure
map stores data in key-value pairs but the only difference between map and object is object can 
hold key which are of string types but map can hold key of any datatype.
*/
const mapDemo = new Map();
mapDemo.set("name","prathamesh");
mapDemo.set("age",22);
mapDemo.set(101,"Id");
console.log(mapDemo);

console.log(mapDemo.get(101));//retrieve data from data on the basis of key


/*
methods 
To understand it,method is function inside object
*/
const obj = {
    firstName : "piyush",
    age : 21,
    about : function(){
        console.log(`Name is ${this.firstName} and age is ${this.age}`);
    }
}
obj.about();
//=================================
const myFunction2 = function(){
    console.log(`Name is ${this.firstName} and age is ${this.age}`);
}
const obj2 = {
    firstName : "anu",
    age : 15,
    about : myFunction2
}

const obj3 = {
    firstName : "suresh",
    age : 31,
    about : myFunction2
}
obj2.about();

/*
accessing value in assigned object
*/
const obj5= {
    key1: "first_value",
    key2: "second_value"
}

const obj4 = {
    key3 : "third_value"
}
console.log(obj4.key3);//value3
console.log(obj4.key2);//undefined cz key2 is not present in obj4

const obj6 = Object.create(obj5);
console.log(obj6.key1);/*it searches for key1 if it is present in obj6 then it'll print is value 
otherwise it'll search in assigned object*/


/*
super keyword
*/
class Car {
    constructor(name, age){
        this.name = name;
        this.age = age;
    }

    running(){
        return `${this.name} is running`;
    }

    isNew(){
        return this.age <= 1;
    }

    isNewest(){
        return true;
    }
}

class Maruti extends Car{
    constructor(name, age, speed){
        super(name,age);
        this.speed = speed;
    }

    run(){
        return `${this.name} is running at ${this.speed}kmph`
    }
} 
// object / instance 
const alto = new Maruti("alto", 3,45);
console.log(alto.run());
console.log(alto.running());


/*
getters setters
*/
class person_{
constructor(fname , lname , age){
    this.fname = fname,
    this.lname = lname,
    this.age = age
}

static information(){
    console.log("This is static method");
}
get fullName_(){//Due to get keyword we can treat fullName_() as a property instead of function
    return `${this.fname} ${this.lname}`;
}

set fullName(fullName){
    const [firstName, lastName] = fullName.split(" ");
    this.fname = firstName;
    this.lname = lastName;
}
}

const person1 = new person_("pratham","londhe",22);
console.log(person1.fullName_);
// console.log(person1.fullName_());
person_.information();